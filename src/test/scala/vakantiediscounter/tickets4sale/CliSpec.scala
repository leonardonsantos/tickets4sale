package vakantiediscounter.tickets4sale

import org.scalatest._

class CliSpec extends WordSpec with Matchers {
  "Cli" should {
    "query shows" in {
      val expected = """{"inventory":[{"genre":"MUSICAL","shows":[{"title":"BEAUTIFUL - THE CAROLE KING MUSICAL ","tickets_left":"90","tickets_available":"10","status":"open for sale"}]}]}"""
      Cli.getResponse("shows.csv", "2019-09-02", "2019-09-15")(true).unsafeRunSync() shouldEqual expected
    }

    "return empty" in {
      val expected = """{"inventory":[]}"""
      Cli.getResponse("shows.csv", "2021-01-01", "2021-01-14")(true).unsafeRunSync() shouldEqual expected
    }
  }
}