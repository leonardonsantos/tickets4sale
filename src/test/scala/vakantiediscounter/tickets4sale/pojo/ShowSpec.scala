package vakantiediscounter.tickets4sale.pojo

import java.util.Date
import org.scalatest._
import Hall._
import Status._

class ShowSpec extends WordSpec with Matchers {
  "parseCsvLineRaw" should {
    "not parse empty line" in {
      Show.parseCsvLineRaw("") shouldEqual None
    }

    "not parse CSV line out of pattern" in {
      val input1 = """"1984","2019-10-14","DRAMA""""
      Show.parseCsvLineRaw(input1) shouldEqual None

      val input2 = """"1984",2019-10-14,"DRAMA","""
      Show.parseCsvLineRaw(input2) shouldEqual None

      val input3 = """"1984",2019-10-14,"""
      Show.parseCsvLineRaw(input3) shouldEqual None
    }

    "parse CSV line" in {
      val input = """"1984",2019-10-14,"DRAMA""""
      val expected = ShowRaw("1984", "2019-10-14", "DRAMA")
      Show.parseCsvLineRaw(input) shouldEqual Some(expected)
    }

    "parse CSV line with comma" in {
      val input = """"BEAUX' STRATAGEM, THE",2019-12-03,"COMEDY""""
      val expected = ShowRaw("BEAUX' STRATAGEM, THE", "2019-12-03", "COMEDY")
      Show.parseCsvLineRaw(input) shouldEqual Some(expected)
    }
  }

  "parseFromRaw" should {
    "not parse malformed date" in {
      val input = ShowRaw("1984", "9999-99-99", "DRAMA")
      Show.parseFromRaw(input) shouldEqual None
    }

    "parse ShowRaw" in {
      val input = ShowRaw("1984", "2019-10-14", "DRAMA")
      val dateExpected = new Date(1571007600000L); //2019-10-13T23:00:00+00:00
      val expected = Show("1984", dateExpected, "DRAMA")
      Show.parseFromRaw(input) shouldEqual Some(expected)
    }
  }

  "status" should {
    val show = Show.parseFromRaw(ShowRaw("TITLE", "2019-09-26", "GENRE")).get

    "be sale not started for the first day" in {
      show.status("2019-08-31", "2019-09-26") shouldEqual "sale not started"
      show.status("2019-08-01", "2019-09-26") shouldEqual "sale not started"
    }
    "be open for sale for the first day" in {
      show.status("2019-09-01", "2019-09-26") shouldEqual "open for sale"
      show.status("2019-09-15", "2019-09-26") shouldEqual "open for sale"
      show.status("2019-09-20", "2019-09-26") shouldEqual "open for sale"
    }
    "be sold out for the first day" in {
      show.status("2019-09-21", "2019-09-26") shouldEqual "sold out"
      show.status("2019-09-25", "2019-09-26") shouldEqual "sold out"
      show.status("2019-09-26", "2019-09-26") shouldEqual "sold out"
    }
    "be in the past for the first day" in {
      show.status("2019-09-27", "2019-09-26") shouldEqual "in the past"
      show.status("2019-10-25", "2019-09-26") shouldEqual "in the past"
    }

    "be sale not started for the second day" in {
      show.status("2019-09-01", "2019-09-27") shouldEqual "sale not started"
      show.status("2019-08-02", "2019-09-27") shouldEqual "sale not started"
    }
    "be open for sale for the second day" in {
      show.status("2019-09-02", "2019-09-27") shouldEqual "open for sale"
      show.status("2019-09-16", "2019-09-27") shouldEqual "open for sale"
      show.status("2019-09-21", "2019-09-27") shouldEqual "open for sale"
    }
    "be sold out for the second day" in {
      show.status("2019-09-22", "2019-09-27") shouldEqual "sold out"
      show.status("2019-09-26", "2019-09-27") shouldEqual "sold out"
      show.status("2019-09-27", "2019-09-27") shouldEqual "sold out"
    }
    "be in the past for the second day" in {
      show.status("2019-09-28", "2019-09-27") shouldEqual "in the past"
      show.status("2019-10-26", "2019-09-27") shouldEqual "in the past"
    }

    "be sale not started for the 100th day" in {
      show.status("2019-12-08", "2020-01-03") shouldEqual "sale not started"
      show.status("2019-11-01", "2020-01-03") shouldEqual "sale not started"
    }
    "be open for sale for the 100th day" in {
      show.status("2019-12-09", "2020-01-03") shouldEqual "open for sale"
      show.status("2019-12-23", "2020-01-03") shouldEqual "open for sale"
      show.status("2019-12-28", "2020-01-03") shouldEqual "open for sale"
    }
    "be sold out for the 100th day" in {
      show.status("2019-12-29", "2020-01-03") shouldEqual "sold out"
      show.status("2020-01-02", "2020-01-03") shouldEqual "sold out"
      show.status("2020-01-03", "2020-01-03") shouldEqual "sold out"
    }
    "be in the past for the 100th day" in {
      show.status("2020-01-04", "2020-01-03") shouldEqual "in the past"
      show.status("2020-02-01", "2020-01-03") shouldEqual "in the past"
    }

    "be in the past for the 101th day" in {
      show.status("2019-12-08", "2020-01-04") shouldEqual "in the past"
      show.status("2019-11-01", "2020-01-04") shouldEqual "in the past"
      show.status("2019-12-09", "2020-01-04") shouldEqual "in the past"
      show.status("2019-12-23", "2020-01-04") shouldEqual "in the past"
      show.status("2019-12-28", "2020-01-04") shouldEqual "in the past"
      show.status("2019-12-29", "2020-01-04") shouldEqual "in the past"
      show.status("2020-01-02", "2020-01-04") shouldEqual "in the past"
      show.status("2020-01-03", "2020-01-04") shouldEqual "in the past"
      show.status("2020-01-04", "2020-01-04") shouldEqual "in the past"
      show.status("2020-02-01", "2020-01-04") shouldEqual "in the past"
    }

    "of opening day in far future" in {
      val show = Show.parseFromRaw(ShowRaw("A MIDSUMMER NIGHT’S DREAM - IN NEW ORLEANS", "2020-04-28", "DRAMA")).get
      show.status("2018-08-01", "2018-08-15") shouldEqual "not showing yet"
    }

  }

  "hall" should {
    val show = Show.parseFromRaw(ShowRaw("TITLE", "2019-09-26", "GENRE")).get

    "big hall" in {
      show.hall("2019-09-26") shouldEqual BIGHALL
      show.hall("2019-09-30") shouldEqual BIGHALL
      show.hall("2019-11-24") shouldEqual BIGHALL
    }

    "small hall" in {
      show.hall("2019-11-25") shouldEqual SMALLHALL // +59 days
      show.hall("2019-12-14") shouldEqual SMALLHALL // +79 days
      show.hall("2019-12-15") shouldEqual SMALLHALL // +80 days
      show.hall("2019-12-15") shouldEqual SMALLHALL // +99 days
    }

    "in the past" in {
      show.hall("2020-01-04") shouldEqual NOHALL // +100 days
    }
  }

  "totalTickets" should {
    val show = Show.parseFromRaw(ShowRaw("TITLE", "2019-09-26", "GENRE")).get

    "Tickets for big hall" in {
      show.totalTickets("2019-09-26") shouldEqual 200
      show.totalTickets("2019-09-30") shouldEqual 200
      show.totalTickets("2019-11-24") shouldEqual 200
    }

    "Tickets for small hall" in {
      show.totalTickets("2019-11-25") shouldEqual 100 // +59 days
      show.totalTickets("2019-12-14") shouldEqual 100 // +79 days
      show.totalTickets("2019-12-15") shouldEqual 100 // +80 days
      show.totalTickets("2019-12-15") shouldEqual 100 // +99 days
    }

    "Tickets for show in the past" in {
      show.totalTickets("2020-01-04") shouldEqual 0 // +100 days
    }
  }

  "availableTickets" should {
    val show = Show.parseFromRaw(ShowRaw("TITLE", "2019-09-26", "GENRE")).get

    "open for sale in big hall" in {
      show.availableTickets(OPENFORSALE, BIGHALL) shouldEqual 10
    }

    "open for sale in small hall" in {
      show.availableTickets(OPENFORSALE, SMALLHALL) shouldEqual 5
    }

    "not available in any other conditions" in {
      show.availableTickets(OPENFORSALE, NOHALL) shouldEqual 0
      show.availableTickets(SALENOTSTARTED, BIGHALL) shouldEqual 0
      show.availableTickets(SALENOTSTARTED, SMALLHALL) shouldEqual 0
      show.availableTickets(SOLDOUT, BIGHALL) shouldEqual 0
      show.availableTickets(SOLDOUT, SMALLHALL) shouldEqual 0
      show.availableTickets(INTHEPAST, BIGHALL) shouldEqual 0
      show.availableTickets(INTHEPAST, SMALLHALL) shouldEqual 0
    }

  }

  "ticketsSold" should {
    val show = Show.parseFromRaw(ShowRaw("TITLE", "2018-08-15", "GENRE")).get

    "have not sold tickets to big hall yet" in {
      show.soldTickets("2018-07-16", "2018-08-15") shouldEqual 0 // -30 days
      show.soldTickets("2018-07-22", "2018-08-15") shouldEqual 0 // -24 days
    }

    "have sold some tickets to big hall" in {
      show.soldTickets("2018-07-23", "2018-08-15") shouldEqual 10 // -23 days
      show.soldTickets("2018-08-01", "2018-08-15") shouldEqual 100 // -14 days
      show.soldTickets("2018-08-02", "2018-08-15") shouldEqual 110 // -15 days
    }

    "have sold out to big hall" in {
      show.soldTickets("2018-08-10", "2018-08-15") shouldEqual 200 // -5 days
      show.soldTickets("2018-08-10", "2018-08-15") shouldEqual 200 // -4 days
      show.soldTickets("2018-08-15", "2018-08-15") shouldEqual 200 // 0 days
      show.soldTickets("2018-08-16", "2018-08-15") shouldEqual 200 // +1 days
    }

    val show2 = Show.parseFromRaw(ShowRaw("TITLE2", "2018-06-11", "GENRE2")).get // -65 days of show1

    "have not sold tickets to small hall yet" in {
      show2.soldTickets("2018-07-16", "2018-08-15") shouldEqual 0 // -30 days
      show2.soldTickets("2018-07-22", "2018-08-15") shouldEqual 0 // -24 days
    }

    "have sold some tickets to small hall" in {
      show2.soldTickets("2018-07-23", "2018-08-15") shouldEqual 5 // -23 days
      show2.soldTickets("2018-08-01", "2018-08-15") shouldEqual 50 // -14 days
      show2.soldTickets("2018-08-02", "2018-08-15") shouldEqual 55 // -15 days
    }

    "have sold out to small hall" in {
      show2.soldTickets("2018-08-10", "2018-08-15") shouldEqual 100 // -5 days
      show2.soldTickets("2018-08-10", "2018-08-15") shouldEqual 100 // -4 days
      show2.soldTickets("2018-08-15", "2018-08-15") shouldEqual 100 // 0 days
      show2.soldTickets("2018-08-16", "2018-08-15") shouldEqual 100 // +1 days
    }
  }

  "basePrice" should {
    "MUSICAL" in {
      val show = Show.parseFromRaw(ShowRaw("TITLE", "2018-08-15", "MUSICAL")).get
      show.basePrice shouldEqual 70.0
    }
    "COMEDY" in {
      val show = Show.parseFromRaw(ShowRaw("TITLE", "2018-08-15", "COMEDY")).get
      show.basePrice shouldEqual 50.0
    }
    "DRAMA" in {
      val show = Show.parseFromRaw(ShowRaw("TITLE", "2018-08-15", "DRAMA")).get
      show.basePrice shouldEqual 40.0
    }
    "any other genre" in {
      val show = Show.parseFromRaw(ShowRaw("TITLE", "2018-08-15", "GENRE")).get
      show.basePrice shouldEqual 70.0
    }
  }

  "price" should {
    val show = Show.parseFromRaw(ShowRaw("TITLE", "2019-09-26", "MUSICAL")).get

    "be base price, if less than 80 days" in {
      show.price("2019-09-26") shouldEqual show.basePrice // 0 days
      show.price("2019-10-06") shouldEqual show.basePrice // +10 days
      show.price("2019-11-25") shouldEqual show.basePrice // +60 days
      show.price("2019-12-14") shouldEqual show.basePrice // +79 days
    }

    "have 20% of discount, if more than 80 days" in {
      show.price("2019-12-15") shouldEqual show.basePrice * 0.80 // +80 days
      show.price("2019-12-16") shouldEqual show.basePrice * 0.80 // +81 days
      show.price("2019-12-25") shouldEqual show.basePrice * 0.80 // +90 days
    }
  }
}