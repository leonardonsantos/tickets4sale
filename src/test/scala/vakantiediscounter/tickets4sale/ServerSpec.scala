package vakantiediscounter.tickets4sale

import org.scalatest._
import akka.http.scaladsl.model.{ContentTypes, StatusCodes}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.http.scaladsl.server._
import Directives._

class ServerSpec extends WordSpec with Matchers with ScalatestRouteTest {
  "Server" should {
    "test something" in {
      Get("/") ~> Server.routes ~> check {
        responseAs[String] shouldEqual "Tickets4Sale"
      }
    }

    "get inventory" in {
      Get("/inventory") ~> Server.routes ~> check {
        responseAs[String].take(2) shouldEqual "[{"
        responseAs[String].takeRight(2) shouldEqual "}]"
      }
    }

    "post blank" in {
      Post("/shows") ~> Server.routes ~> check {
        status should ===(StatusCodes.BadRequest)
      }
    }

    "post something acceptable" in {
      val body: String = """{"show-date": "2019-10-02"}"""
      Post("/shows", body) ~> Server.routes ~> check {
        status should ===(StatusCodes.OK)
        contentType should ===(ContentTypes.`application/json`)
      }
    }

  }
}