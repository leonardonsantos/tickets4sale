package vakantiediscounter.tickets4sale.helper

import cats.effect.IO
import vakantiediscounter.tickets4sale.pojo.{Show, ShowRaw}

object CsvFile {
  def readFile(filename: String, fromResource: Boolean = false): IO[Seq[String]] = {
    IO {
      if (fromResource) {
        io.Source.fromResource(filename)
      } else {
        io.Source.fromFile(filename)
      }
    }.bracket { bufferedSource => {
      IO {
        val lines = for (line <- bufferedSource.getLines()) yield (line)
        lines.toVector
      }
    }} { bufferedSource => {
      IO(bufferedSource.close)
    }}
  }

  def parseCsvLines(lines: Seq[String]): Seq[Show] = {
    for {
      line <- lines
      showRaw <- Show.parseCsvLineRaw(line)
      show <- Show.parseFromRaw(showRaw)
    } yield (show)
  }
}
