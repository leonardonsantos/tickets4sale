package vakantiediscounter.tickets4sale

import cats.effect.IO
import vakantiediscounter.tickets4sale.pojo.{QueryShow, Show}
import vakantiediscounter.tickets4sale.pojo.Status._
import org.json4s._
import org.json4s.jackson.Serialization.write

object Cli {
  case class ShowOutput(title: String, tickets_left: String, tickets_available: String, status: String)
  case class ShowsByGenre(genre: String, shows: Seq[ShowOutput])
  case class InventoryOutput(inventory: Seq[ShowsByGenre])

  def prepareShowOutput(queryShow: QueryShow): ShowOutput = {
    ShowOutput(
      title = queryShow.show.title,
      tickets_left = queryShow.leftTickets.toString,
      tickets_available = queryShow.availableTickets.toString,
      status = queryShow.stausString
    )
  }

  def prepareOutput(groupedByGenre: Map[String, Seq[QueryShow]]): InventoryOutput = {
    val inventory = groupedByGenre.toSeq.map{ case (k,v) => {
      val genre = k
      val showsOutput = v.map(prepareShowOutput)
      ShowsByGenre(genre, showsOutput)
    }}
    InventoryOutput(inventory)
  }

  def inventoryToJson(inventory: InventoryOutput): String = {
    write(inventory)(DefaultFormats)
  }

  def getResponse(csvFile: String, queryDate: String, showDate: String)(implicit fromResource: Boolean = false): IO[String] = {
    for {
      lines <- helper.CsvFile.readFile(csvFile, fromResource)
      shows = helper.CsvFile.parseCsvLines(lines)
      queriesShow = QueryShow.prepareQueriesShow(shows, queryDate, showDate)
      groupedByGenre = QueryShow.groupByGenre(queriesShow)
      inventoryOutput = prepareOutput(groupedByGenre)
      json = inventoryToJson(inventoryOutput)
    } yield (json)
  }

  def run(csvFile: String, queryDate: String, showDate: String)(implicit fromResource: Boolean = false): IO[Unit] = {
    for {
      json <- getResponse(csvFile, queryDate, showDate)(fromResource)
      _ <- IO(println(json))
    } yield ()
  }

  def main(args: Array[String]): Unit = {
    val io = if (args.length != 3) {
        IO(println("Usage: Cli <SHOWS.CSV> <QUERY-DATE> <SHOW-DATE>"))
      } else {
        run(args(0), args(1), args(2))
      }

    io.unsafeRunSync()
  }
}
