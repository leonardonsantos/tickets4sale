package vakantiediscounter.tickets4sale.pojo

import java.text.SimpleDateFormat
import java.util.{Date, TimeZone}

import scala.util.Try
import Status._
import Hall._

case class ShowRaw(title: String, openingDay: String, genre: String)

case class Show(title: String, openingDay: Date, genre: String) {
  def status(queryDate: String, showDate: String): String = {
    val qDate = Show.parseDate(queryDate)
    val sDate = Show.parseDate(showDate)

    Status.enumarationToString(status(qDate, sDate))
  }

  def status(qDate: Date, sDate: Date): Status = {
    val diffDaysFromOpeningDay = Show.diffDays(sDate, openingDay)
    val diffDaysFromQueryDate = Show.diffDays(qDate, sDate)

    if (diffDaysFromOpeningDay < 0) {
      NOTSHOWINGYET
    } else if (diffDaysFromOpeningDay >= 100) {
      INTHEPAST
    } else if (diffDaysFromQueryDate < -25 ) {
      SALENOTSTARTED
    } else if ((-25 until -5) contains diffDaysFromQueryDate) {
      OPENFORSALE
    } else if ((-5 to 0) contains diffDaysFromQueryDate) {
      SOLDOUT
    } else {
      INTHEPAST
    }
  }

  def hall(showDate: String): Hall = {
    val sDate = Show.parseDate(showDate)
    hall(sDate)
  }

  def hall(sDate: Date): Hall = {
    val diffDays = Show.diffDays(sDate, openingDay)

    if (diffDays < 60) {
      BIGHALL
    } else if (diffDays < 100) {
      SMALLHALL
    } else {
      NOHALL
    }
  }

  def totalTickets(showDate: String): Int = {
    val sDate = Show.parseDate(showDate)
    totalTickets(sDate)
  }

  def totalTickets(sDate: Date): Int = {
    hall(sDate) match {
      case BIGHALL => 200
      case SMALLHALL => 100
      case _ => 0
    }
  }

  def availableTickets(queryDate: String, showDate: String): Int = {
    val qDate = Show.parseDate(queryDate)
    val sDate = Show.parseDate(showDate)

    val s = status(qDate, sDate)
    val h = hall(sDate)

    availableTickets(s, h)
  }

  def availableTickets(status: Status, hall: Hall): Int = {
    (status, hall) match {
      case (OPENFORSALE, BIGHALL) => 10
      case (OPENFORSALE, SMALLHALL) => 5
      case _ => 0
    }
  }

  def soldTickets(queryDate: String, showDate: String): Int = {
    val qDate = Show.parseDate(queryDate)
    val sDate = Show.parseDate(showDate)
    soldTickets(qDate, sDate)
  }

  def soldTickets(qDate: Date, sDate: Date): Int = {
    val s = status(qDate, sDate)
    val h = hall(sDate)
    val availableT = availableTickets(s, h)
    val tottalT = totalTickets(sDate)
    val diffDaysFromQueryDate = Show.diffDays(qDate, sDate)

    s match {
      case SALENOTSTARTED => 0
      case OPENFORSALE => ((25 + diffDaysFromQueryDate - 1) * availableT).toInt
      case _ => tottalT
    }
  }

  def leftTickets(queryDate: String, showDate: String): Int = {
    val qDate = Show.parseDate(queryDate)
    val sDate = Show.parseDate(showDate)

    leftTickets(qDate, sDate)
  }

  def leftTickets(qDate: Date, sDate: Date): Int = {
    totalTickets(sDate) - soldTickets(qDate, sDate)
  }

  def basePrice: Double = {
    genre match {
      case "MUSICAL" => 70.0
      case "COMEDY" => 50.0
      case "DRAMA" => 40.0
      case _ => 70.0
    }
  }

  def price(showDate: String): Double = {
    val sDate = Show.parseDate(showDate)
    price(sDate)
  }

  def price(sDate: Date): Double = {
    val diffDays = Show.diffDays(sDate, openingDay)
    val discount = 0.20

    if (diffDays < 80) {
      basePrice
    } else {
      basePrice * (1.0 - discount)
    }
  }
}

object Show {
  val dateFormat = "yyyy-MM-dd"

  def dateFormatObject(): SimpleDateFormat = {
    val format = new SimpleDateFormat(dateFormat)
    format.setLenient(false);
    format.setTimeZone(TimeZone.getTimeZone("GMT+1")) // standard time for Amsterdam
    format
  }

  def parseCsvLineRaw(line: String): Option[ShowRaw] = {
    // TODO: Use some library to parse CSV, handle escaped strings, etc.
    val linePattern = "\"([^\"]+)\",(\\d\\d\\d\\d-\\d\\d-\\d\\d),\"([^\"]+)\"".r

    line match {
      case linePattern(title, openingDay, genre) => Some(ShowRaw(title, openingDay, genre))
      case _ => None
    }
  }

  def parseDate(dateString: String): Date = {
    dateFormatObject().parse(dateString)
  }

  def diffDays(date1: Date, date2: Date): Long = {
    (date1.getTime() - date2.getTime()) / (1000 * 60 * 60 * 24)
  }

  def getToday(): String = {
    dateFormatObject().format(new Date());
  }

  def parseFromRaw(showRaw: ShowRaw): Option[Show] = {
    Try {
      val openingDay = parseDate(showRaw.openingDay)
      Show(showRaw.title, openingDay, showRaw.genre)
    }.toOption
  }
}