package vakantiediscounter.tickets4sale.pojo

object Hall extends Enumeration {
  type Hall = Value
  val BIGHALL, SMALLHALL, NOHALL = Value
}
