package vakantiediscounter.tickets4sale.pojo

import Status._

case class QueryShow(show: Show, queryDate: String, showDate: String) {
  val qDate = Show.parseDate(queryDate)
  val sDate = Show.parseDate(showDate)

  val status = show.status(qDate, sDate)
  val stausString = Status.enumarationToString(status)
  val hall = show.hall(sDate)

  val totalTickets = show.totalTickets(sDate)
  val leftTickets = show.leftTickets(qDate, sDate)
  val availableTickets = show.availableTickets(status, hall)

  val basePrice = show.basePrice
  val price = show.price(sDate)
}

object QueryShow {
  def prepareQueriesShow(shows: Seq[Show], queryDate: String, showDate: String): Seq[QueryShow] = {
    shows
      .map( show => QueryShow(show, queryDate, showDate) )
      .filterNot( s => s.status == NOTSHOWINGYET || s.status == INTHEPAST)
  }

  def groupByGenre(queriesShow: Seq[QueryShow]): Map[String, Seq[QueryShow]] = {
    queriesShow.groupBy(_.show.genre)
  }
}