package vakantiediscounter.tickets4sale.pojo

object Status extends Enumeration {
  type Status = Value
  val NOTSHOWINGYET, SALENOTSTARTED, OPENFORSALE, SOLDOUT, INTHEPAST = Value

  def enumarationToString(status: Value): String = {
    status match {
      case NOTSHOWINGYET => "not showing yet"
      case SALENOTSTARTED => "sale not started"
      case OPENFORSALE => "open for sale"
      case SOLDOUT => "sold out"
      case INTHEPAST => "in the past"
    }
  }
}