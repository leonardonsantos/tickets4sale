package vakantiediscounter.tickets4sale

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.ContentTypes._
import akka.http.scaladsl.model.headers.`Content-Type`
import akka.stream.ActorMaterializer
import org.json4s._
import org.json4s.jackson.JsonMethods._
import org.json4s.jackson.Serialization.write
import vakantiediscounter.tickets4sale.pojo.{QueryShow, Show}
import vakantiediscounter.tickets4sale.pojo.Status._

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success, Try}

case class RequestPost(`show-date`: String)

object Server {
  val csvFile = "shows.csv"
  val host = "0.0.0.0"
  val port = 9000
  implicit val system: ActorSystem = ActorSystem("vakantiediscounter")
  implicit val executor: ExecutionContext = system.dispatcher
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val formats = DefaultFormats

  val shows: Seq[Show] = (for {
    lines <- helper.CsvFile.readFile(csvFile, true)
    shows = helper.CsvFile.parseCsvLines(lines)
  } yield (shows)).unsafeRunSync()

  case class ShowOutput(title: String, tickets_left: String, tickets_available: String, status: String, price: String)
  case class ShowsByGenre(genre: String, shows: Seq[ShowOutput])
  case class InventoryOutput(inventory: Seq[ShowsByGenre])

  def prepareShowOutput(queryShow: QueryShow): ShowOutput = {
    ShowOutput(
      title = queryShow.show.title,
      tickets_left = queryShow.leftTickets.toString,
      tickets_available = queryShow.availableTickets.toString,
      status = queryShow.stausString,
      price = queryShow.price.toInt.toString
    )
  }

  def queryShows(showDate: String): InventoryOutput = {
    val today = Show.getToday()
    val queriesShow = QueryShow.prepareQueriesShow(shows, today, showDate)
    val groupedByGenre = QueryShow.groupByGenre(queriesShow)
    val inventory = groupedByGenre.toSeq.map{ case (k,v) => {
      val genre = k
      val showsOutput = v.map(prepareShowOutput)
      ShowsByGenre(genre, showsOutput)
    }}
    InventoryOutput(inventory)
  }

  lazy val routes: Route = {
    path("") {
      get {
        complete("Tickets4Sale")
      }
    } ~
    path("inventory") {
      get {
        val entity = HttpEntity(ContentTypes.`application/json`, write(shows))
        val res = HttpResponse(StatusCodes.OK, entity = entity)
        complete(res)
      }
    } ~
    path("shows") {
      post {
        entity(as[String]) { str =>
          val input = Try {
            parse(str).extract[RequestPost]
          }

          if (input.isSuccess) {
            val showDate = input.get.`show-date`
            val response = queryShows(showDate)

            val entity = HttpEntity(ContentTypes.`application/json`, write(response))
            val res = HttpResponse(StatusCodes.OK, entity = entity)
            complete(res)
          } else {
            complete(400, "bad request")
          }
        }
      }
    }
  }

  def main(args: Array[String]): Unit = {
    val bindingFuture = Http().bindAndHandle(routes, host, port)

    bindingFuture.onComplete {
      case Success(serverBinding) => println(s"listening to ${serverBinding.localAddress}")
      case Failure(error) => println(s"error: ${error.getMessage}")
    }
  }
}
