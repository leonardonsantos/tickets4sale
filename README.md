# Assessment Dé VakantieDiscounter

Running tests:

```
$ sbt test
```

Assembly jar:

```
$ sbt assembly
```

## Cli

Running with sbt:

```
$ sbt "runMain vakantiediscounter.tickets4sale.Cli src/main/resources/shows.csv 2020-01-01 2020-01-14"
```

Running with jar:

```
$ java -cp target/scala-2.13/tickets4sale-assembly-0.1.jar vakantiediscounter.tickets4sale.Cli src/main/resources/shows.csv 2020-01-01 2020-01-14
```

## Server

Running with sbt:

```
$ sbt "runMain vakantiediscounter.tickets4sale.Server"
```

Running with jar:

```
$ java -cp target/scala-2.13/tickets4sale-assembly-0.1.jar vakantiediscounter.tickets4sale.Server
```

Making request:

```
$ curl -sv -X POST -d '{"show-date": "2019-10-15"}' http://localhost:9000/shows
```

**The problem definition says that the input should be `{"show-date": 2019-10-15}` but this is not a valid json.*